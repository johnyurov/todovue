import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    Auntificated: false
  },
  getters: {
    VALUE: state => {
      return state.Auntificated
    },
  },
  mutations: {
    isAuntificated(state) {
      state.Auntificated = true
    },
    isNotAuntificated(state) {
      state.Auntificated = false
    }
  },
  actions: {
  },
  modules: {
  }
})
