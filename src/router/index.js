import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/index.js'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: () => import('@/views/dashboard/Dashboard.vue'),
    meta: {
      requiresAuth: true,
      layout: 'horizontal',
    },
  },
  {
    path: '/create',
    name: 'Createtodo',
    component: () => import('@/views/todo/CreateToDo.vue'),
    meta: {
      requiresAuth: true,
      layout: 'horizontal',
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('@/views/profile/Profile.vue'),
    meta: {
      requiresAuth: true,
      layout: 'horizontal',
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/authorization/Authorization.vue'),
    meta: {
      requiresAuth: false,
      layout: 'full',
    }
  },
  {
    path: '/registration',
    name: 'Registration',
    component: () => import('@/views/authorization/Registration.vue'),
    meta: {
      requiresAuth: false,
      layout: 'full',
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
  if(store.getters.VALUE === false) {
    next({name: 'Login'})
  } else {
    next()
  }
  } else {
    next()
  }
})

export default router
